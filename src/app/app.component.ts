import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `
    <div class="container">

      <div class="center">
        <h1>Weather Forecast</h1>
      </div>
      <div class="box-shadow">
        <div class="center">
          <p>Enter the geo-coordinates for the location you want to have a weather forecast:</p>
        </div>
        <div class="inputRow">
          <div class="inputField">
            <p>Latitude:</p>
            <input id="latitude" #latitude (change)="setValue(latitude.value, 'latitude')">
          </div>
          <div class="inputField">
            <p>Longitude:</p>
            <input id="longitude" #longitude (change)="setValue(longitude.value, 'longitude')">
          </div>
        </div>
        <div class="inputRow">
          <div class="inputField">
            <p>Date & Time (optional):</p>
            <div class="inputField">
              <input id="year" #year (change)="setValue(year.value, 'year')" size="4" maxlength="4" class="fourDigits"> -
              <input id="month" #month (change)="setValue(month.value, 'month')" size="2" maxlength="2" class="twoDigits"> -
              <input id="day" #day (change)="setValue(day.value, 'day')" size="2" maxlength="2" class="twoDigits">
            </div>
            <div class="inputField">
              <input id="hour" #hour (change)="setValue(hour.value, 'hour')" size="2" maxlength="2" class="twoDigits"> :
              <input id="minute" #minute (change)="setValue(minute.value, 'minute')" size="2" maxlength="2" class="twoDigits"> :
              <input id="second" #second (change)="setValue(second.value, 'second')" size="2" maxlength="2" class="twoDigits"><br>
            </div>
            <p>Format: [YYYY]-[MM]-[DD]  [HH]:[MM]:[SS]</p>
          </div>
        </div>
        <div class="center">
          <div id="checkWeatherButton" class="custom-button" (keyup.enter)="showWeatherForecast()" (click)="showWeatherForecast()">Check Weather</div>
        </div>
      </div>

      <div id="forecastContainer" class="center"></div>
      <a href="https://darksky.net/poweredby/">
        <div class="darksky">
          Powered by Dark Sky
        </div>
        <div class="darksky">
          <img class="darksky" src="../assets/darkskylogo.png"/>
        </div>
      </a>
    </div>
  `
})

export class AppComponent {

  title = 'Weather Forecast';
  latitude = "";
  longitude = "";
  year = "";
  month = "";
  day = "";
  hour = "";
  minute = "";
  second = "";

  constructor(private http: HttpClient) {}

  // This function is called when the user clicks "Check Weather" button
  // performs the GET request to obtain the weather data and displaying adequately
  showWeatherForecast() {

    /* Clean up the last session */

    // Remove old forecast DOM elements
    var forecastContainer = document.getElementById("forecastContainer");
    forecastContainer.innerHTML = '';
    // Remove error signs on the input fields
    var errorElements = document.getElementsByClassName("error");
    for (var i = 0; i < errorElements.length; i++) {
        errorElements[i].classList.remove("remove");
    }

    /* Check format of user input */

    // Latitude should only contain numbers and one dot
    var latitudeValid = this.geoCoordinateCheck(this.latitude, "latitude");

    // Longitude should only contain numbers and one dot
    var longitudeValid = this.geoCoordinateCheck(this.longitude, "longitude");

    // Check format of date
    var dateValid = this.dateFormatCheck(this.year, this.month, this.day, this.hour, this.minute, this.second);
    var dateEmpty = this.isDateEmpty(this.year, this.month, this.day, this.hour, this.minute, this.second);

    /* GET request */

    // Only send the GET request if all entered values are valid
    if (latitudeValid && longitudeValid && dateValid) {
      var url = "https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/58f311133e4ce5f548b429406726683e/" + this.latitude + "," + this.longitude

      // If date is not empty, appending the date to the url
      if (!dateEmpty){
        url += "," + this.year + "-" + this.month + "-" + this.day + "T" + this.hour  + ":" + this.minute  + ":" + this.second;
      }

      // Getting temperature in degrees not fahrenheit, therefore SI unit system
      url += "?units=si"

      this.http.get(url).subscribe(
      data => {
        // save timezone to display it later
        var timezone = data["timezone"];

        // Displaying the forecast for the next days
        var dayForecast = data["daily"]["data"];
        var forecast = document.getElementById("forecastContainer");

        for (var i = 0; i < dayForecast.length; i++){

          /* Creating a div for every day of the forecast, which contains some
             information regarding the forecast of that day */
          var tmpDiv = document.createElement("div");
          var box = document.createElement("div");
          tmpDiv.className += "forecast";
          box.className += "box-shadow";
          tmpDiv.appendChild(box);
          forecast.appendChild(tmpDiv);

          // Fill day forecast div with information //

          // Show the date of the forecast
          this.displayDate(dayForecast[i]["time"], box);

          // Show an icon which represents the weather
          this.displayWeatherImage(dayForecast[i]["icon"], box);

          // Show the timezone of the forecast
          this.displayTimezone(timezone, box);

          // Show short weather summary text
          this.displaySummary(dayForecast[i]["summary"], box);

          // Show minimum and maximum temperature
          this.displayTemperature(dayForecast[i]["temperatureMin"].toString(), dayForecast[i]["temperatureMax"].toString(), box);
        }
      },
      (err: HttpErrorResponse) => {
        alert("An error occured when trying to retrieve weather data. Is your internet connection running?");
      });
    }
    else {
      return;
    }
  }

  // Stores a value of the form in member variables of this class
  setValue(input: string, variable: string) {
    switch (variable) {
      case "latitude" :
        this.latitude = input;
        break;
      case "longitude" :
        this.longitude = input;
        break;
      case "year" :
        this.year = input;
        break;
      case "month" :
        this.month = input;
        break;
      case "day" :
        this.day = input;
        break;
      case "hour" :
        this.hour = input;
        break;
      case "minute" :
        this.minute = input;
        break;
      case "second" :
        this.second = input;
        break;
    }
  }

  geoCoordinateCheck (coordinate: string, element: string) {
    if(coordinate.match(/^\d+\.?\d*$/)){
      return true;
    }
    document.getElementById(element).className = "error";
    alert("Invalid latitude value. Only decimal numbers allowed. E.g.: 25.64");
    return false;
  }

  dateFormatCheck(year: string, month: string, day: string, hour:string,
    minute: string, second: string){

    var dateValid = false;
    var dateEmpty = false;
    // No time values is a valid request
    if (this.isDateEmpty(year, month, day, hour, minute, second)) {
      return true;
    }
    // If there are date values, we have to check the format of them
    else {
      // At first the value is true
      dateValid = true;
      if (year.match(/\d{4}/) == null){
        document.getElementById("year").className += " error";
        dateValid = false;
      }
      if (month.match(/[01]\d/) == null || parseInt(month) > 12) {
        document.getElementById("month").className += " error";
        dateValid = false;
      }
      if (!day.match(/[0-3][0-9]/) || parseInt(day) > 31) {
        document.getElementById("day").className += " error";
        dateValid = false;
      }
      if (!hour.match(/[0-2][0-9]/) || parseInt(hour) > 23) {
        document.getElementById("hour").className += " error";
        dateValid = false;
      }
      if (!minute.match(/[0-6]\d/) || parseInt(hour) > 59) {
        document.getElementById("minute").className += " error";
        dateValid = false;
      }
      if (!second.match(/[0-6]\d/) || parseInt(hour) > 59) {
        document.getElementById("second").className += " error";
        dateValid = false;
      }

      // Requests which extend the year 2038 and before 1950 are invalid
      var yearInt = parseInt(year);
      if (yearInt < 1950 || yearInt > 2037) {
        dateValid = false;
        document.getElementById("year").className += " error";
        alert("Year value has to be in the range of 1950 and 2038")
      }

      /* Filtering special cases:
         February (only 28 days. In leap year 29)
         February, April, July, September and November (only 30 days) */
      var monthInt = parseInt(month);
      var dayInt = parseInt(day);
      if (monthInt == 2 && dayInt > 28) {
        // leap year and 29 days is ok
        var leapYear = ((yearInt % 4 == 0) && (yearInt % 100 != 0)) || (yearInt % 400 == 0)
        if (!(leapYear && dayInt == 29)){
          document.getElementById("day").className += " error";
          document.getElementById("month").className += " error";
          alert("February has only 28 days");
        }
      } else if (dayInt == 31 &&
      (monthInt == 2 || monthInt == 4 || monthInt == 6 || monthInt == 9 || monthInt == 11)) {
        dateValid = false;
        document.getElementById("day").className += " error";
        document.getElementById("month").className += " error";
        alert("In this month no 31st exists");
      }

      // Print error message
      if (!dateValid){
        alert("One or more date values are invalid")
      }
      return dateValid;
    }
  }

  isDateEmpty (year: string, month: string, day: string, hour:string,
    minute: string, second: string){
    if (year == "" && month == "" && day == "" &&
    hour == "" && minute == "" && second == "") {
      return true;
    }
    return false;
  }

  displayTimezone (timezone, location) {
    var tmp = document.createElement("h3");
    tmp.innerHTML = timezone;
    location.appendChild(tmp);
  }

  displayDate (time, location) {
    var tmp = document.createElement("h2");

    var date = new Date(time*1000);
    var timeString = date.getDate().toString() + ".";
    timeString += (date.getMonth() + 1) + ".";
    timeString += date.getFullYear();
    tmp.innerHTML = timeString;
    location.appendChild(tmp);

  }

  displaySummary (summary, location) {
    var tmp = document.createElement("div");

    tmp.innerHTML = summary.toString();
    location.appendChild(tmp);
  }

  displayTemperature (min, max, location) {
    var tmp = document.createElement("div");
    var tmp2 = document.createElement("div");

    tmp.innerHTML = "Min. Temperature: " + min.substring(0, min.indexOf(".")) + " °<br>";
    tmp2.innerHTML = "Max. Temperature: " + max.substring(0, max.indexOf(".")) + " °<br>";

    location.appendChild(tmp);
    location.appendChild(tmp2);
  }

  displayWeatherImage(icon: string, location){

    var tmp = document.createElement("img");

    if(icon.indexOf("partly-cloudy") != -1){
      tmp.src = "../assets/partly-cloudy.png";
    }
    else if (icon.indexOf("cloudy") != -1){
      tmp.src = "../assets/cloudy.png";
    }
    else if (icon.indexOf("rain") != -1){
      tmp.src = "../assets/rainy.png";
    }
    else if (icon.indexOf("dizzle") != -1){
      tmp.src = "../assets/dizzling.png";
    }
    else if (icon.indexOf("storm") != -1){
      tmp.src = "../assets/thunderstorm.png";
    }
    else if (icon.indexOf("clear") != -1){
      tmp.src = "../assets/sunny.png";
    }
    else if (icon.indexOf("fog") != -1){
          tmp.src = "../assets/fog.png";
        }
    tmp.className += " inline";
    location.appendChild(tmp);
  }
}
