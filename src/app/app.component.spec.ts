import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DebugElement } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  it('Should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it("No longitude given", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    button.triggerEventHandler("click", null);
    fixture.detectChanges();

    // input field should show the red error border
    const inputLong = fixture.debugElement.query(By.css('#longitude'));
    var error = inputLong.nativeElement.className.indexOf('error') != -1;

    // forecast elements must not be received and displayed
    // old forecast elements of last request has to be removed
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerEmpty = !forecastContainer.nativeElement.hasChildNodes();

    expect(error && containerEmpty).toBe(true);
  });

  it("No latitude given", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    button.triggerEventHandler("click", null);
    fixture.detectChanges();

    // input field should show the red error border
    const inputLat = fixture.debugElement.query(By.css('#latitude'));
    var error = inputLat.nativeElement.className.indexOf('error') != -1;

    // forecast elements must not be received and displayed
    // forecast elements must not be visible from the last request
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerEmpty = !forecastContainer.nativeElement.hasChildNodes();

    expect(error && containerEmpty).toBe(true);
  });
});

describe('Valid request with latitude and longitude but without time', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));

    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Valid request with latitude and longitude but without time", function() {

    // there should be elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerFilled = forecastContainer.nativeElement.hasChildNodes();
    expect(containerFilled).toBe(true)
  });
});



describe('Valid request with latitude and longitude and time', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));
    inputYear = fixture.debugElement.query(By.css('#year'));
    inputMonth = fixture.debugElement.query(By.css('#month'));
    inputDay = fixture.debugElement.query(By.css('#day'));
    inputHour = fixture.debugElement.query(By.css('#hour'));
    inputMinute = fixture.debugElement.query(By.css('#minute'));
    inputSecond = fixture.debugElement.query(By.css('#second'));
    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    inputYear.nativeElement.value = "2017";
    inputMonth.nativeElement.value = "08";
    inputDay.nativeElement.value = "20";
    inputHour.nativeElement.value = "12";
    inputMinute.nativeElement.value = "00";
    inputSecond.nativeElement.value = "00";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);
    inputYear.triggerEventHandler("change", null);
    inputMonth.triggerEventHandler("change", null);
    inputDay.triggerEventHandler("change", null);
    inputHour.triggerEventHandler("change", null);
    inputMinute.triggerEventHandler("change", null);
    inputSecond.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Valid request with latitude and longitude and time", function() {

    // there should be elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerFilled = forecastContainer.nativeElement.hasChildNodes();
    expect(containerFilled).toBe(true)
  });
});



describe('Date to far in the past', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));
    inputYear = fixture.debugElement.query(By.css('#year'));
    inputMonth = fixture.debugElement.query(By.css('#month'));
    inputDay = fixture.debugElement.query(By.css('#day'));
    inputHour = fixture.debugElement.query(By.css('#hour'));
    inputMinute = fixture.debugElement.query(By.css('#minute'));
    inputSecond = fixture.debugElement.query(By.css('#second'));
    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    inputYear.nativeElement.value = "1945";
    inputMonth.nativeElement.value = "01";
    inputDay.nativeElement.value = "01";
    inputHour.nativeElement.value = "12";
    inputMinute.nativeElement.value = "00";
    inputSecond.nativeElement.value = "00";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);
    inputYear.triggerEventHandler("change", null);
    inputMonth.triggerEventHandler("change", null);
    inputDay.triggerEventHandler("change", null);
    inputHour.triggerEventHandler("change", null);
    inputMinute.triggerEventHandler("change", null);
    inputSecond.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Date to far in the past", function() {

    // there should be no elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerEmpty = forecastContainer.nativeElement.childElementCount == 0;

    // input field should show the red error border
    var error = inputYear.nativeElement.className.indexOf('error') != -1;

    expect(containerEmpty).toBe(true)
  });
});



describe('Date too far in the future', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));
    inputYear = fixture.debugElement.query(By.css('#year'));
    inputMonth = fixture.debugElement.query(By.css('#month'));
    inputDay = fixture.debugElement.query(By.css('#day'));
    inputHour = fixture.debugElement.query(By.css('#hour'));
    inputMinute = fixture.debugElement.query(By.css('#minute'));
    inputSecond = fixture.debugElement.query(By.css('#second'));
    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    inputYear.nativeElement.value = "2038";
    inputMonth.nativeElement.value = "01";
    inputDay.nativeElement.value = "01";
    inputHour.nativeElement.value = "12";
    inputMinute.nativeElement.value = "00";
    inputSecond.nativeElement.value = "00";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);
    inputYear.triggerEventHandler("change", null);
    inputMonth.triggerEventHandler("change", null);
    inputDay.triggerEventHandler("change", null);
    inputHour.triggerEventHandler("change", null);
    inputMinute.triggerEventHandler("change", null);
    inputSecond.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Date too far in the future", function() {

    // there should be no elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerEmpty = forecastContainer.nativeElement.childElementCount == 0;

    // input field should show the red error border
    var error = inputYear.nativeElement.className.indexOf('error') != -1;

    expect(error && containerEmpty).toBe(true);
  });
});



describe('Invalid february date - 29.02.2017', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));
    inputYear = fixture.debugElement.query(By.css('#year'));
    inputMonth = fixture.debugElement.query(By.css('#month'));
    inputDay = fixture.debugElement.query(By.css('#day'));
    inputHour = fixture.debugElement.query(By.css('#hour'));
    inputMinute = fixture.debugElement.query(By.css('#minute'));
    inputSecond = fixture.debugElement.query(By.css('#second'));
    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    inputYear.nativeElement.value = "2017";
    inputMonth.nativeElement.value = "02";
    inputDay.nativeElement.value = "29";
    inputHour.nativeElement.value = "12";
    inputMinute.nativeElement.value = "00";
    inputSecond.nativeElement.value = "00";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);
    inputYear.triggerEventHandler("change", null);
    inputMonth.triggerEventHandler("change", null);
    inputDay.triggerEventHandler("change", null);
    inputHour.triggerEventHandler("change", null);
    inputMinute.triggerEventHandler("change", null);
    inputSecond.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Invalid february date - 29.02.2017", function() {

    // there should be no elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerEmpty = forecastContainer.nativeElement.childElementCount == 0;

    // input field should show the red error border
    var error1 = inputMonth.nativeElement.className.indexOf('error') != -1;
    var error2 = inputDay.nativeElement.className.indexOf('error') != -1;

    expect(error1 && error2 && containerEmpty).toBe(true);
  });
});



describe('Valid february date - 29.02.2000 (leap year test)', function() {
  let fixture: ComponentFixture<AppComponent>;
  let inputLong: DebugElement;
  let inputLat: DebugElement;
  let inputYear: DebugElement;
  let inputMonth: DebugElement;
  let inputDay: DebugElement;
  let inputHour: DebugElement;
  let inputMinute: DebugElement;
  let inputSecond: DebugElement;
  let button: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  beforeEach(function(done) {

    fixture = TestBed.createComponent(AppComponent);
    // get necessary input fields and fill with test values
    inputLong = fixture.debugElement.query(By.css('#longitude'));
    inputLat = fixture.debugElement.query(By.css('#latitude'));
    inputYear = fixture.debugElement.query(By.css('#year'));
    inputMonth = fixture.debugElement.query(By.css('#month'));
    inputDay = fixture.debugElement.query(By.css('#day'));
    inputHour = fixture.debugElement.query(By.css('#hour'));
    inputMinute = fixture.debugElement.query(By.css('#minute'));
    inputSecond = fixture.debugElement.query(By.css('#second'));
    button = fixture.debugElement.query(By.css('#checkWeatherButton'));
    inputLong.nativeElement.value = "12.34";
    inputLat.nativeElement.value = "12.34";
    inputYear.nativeElement.value = "2000";
    inputMonth.nativeElement.value = "02";
    inputDay.nativeElement.value = "29";
    inputHour.nativeElement.value = "12";
    inputMinute.nativeElement.value = "00";
    inputSecond.nativeElement.value = "00";
    // one-time trigger the change-event so that the entered values are used
    inputLong.triggerEventHandler("change", null);
    inputLat.triggerEventHandler("change", null);
    inputYear.triggerEventHandler("change", null);
    inputMonth.triggerEventHandler("change", null);
    inputDay.triggerEventHandler("change", null);
    inputHour.triggerEventHandler("change", null);
    inputMinute.triggerEventHandler("change", null);
    inputSecond.triggerEventHandler("change", null);

    // perform a click on the "Check Weather"-button
    button.triggerEventHandler("click", null);

    setTimeout(function() {
      done();
    }, 2500);
  });

  it("Valid february date - 29.02.2000 (leap year test)", function() {

    // there should be no elements in the forecast container
    const forecastContainer = fixture.debugElement.query(By.css('#forecastContainer'));
    var containerFilled = forecastContainer.nativeElement.childElementCount > 0;

    // input field should show the red error border
    var error1 = inputMonth.nativeElement.className.indexOf('error') == -1;
    var error2 = inputDay.nativeElement.className.indexOf('error') == -1;

    expect(error1 && error2 && containerFilled).toBe(true);
  });
});
